# tmux-addr-extensions

Adds some keybindings and defaults that help usage with the `addr` dotfiles

## Installation

There are two methods to install this.

### Install using ADDR

You can follow the instructions over on the [addr repository](https://gitlab.com/NateDogg1232/addr) to install those dotfiles

### Install using TPM

Make sure you have [tpm](https://github.com/tmux-plugins/tpm) installed. Then, in your tmux.conf file, add the following into your plugin list:

```tmux
set -g @plugin 'git@gitlab.com:NateDogg1232/tmux-addr-extensions'
```

Then, make sure you install using `prefix + I` (Capital I), and you'll be set!
