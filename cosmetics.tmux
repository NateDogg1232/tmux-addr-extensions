#!/bin/sh

setup_status_bar() {
    # Set it to top and center (centre)
    tmux set-option -g status-justify centre
    tmux set-option -g status-position top
}

main() {
    setup_status_bar
}

main

# vim: ft=sh
