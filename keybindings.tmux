#!/bin/sh

setup_status_bar_toggle() {
    tmux bind-key Z "set-option -g status off"
    tmux bind-key z "set-option -g status on"
}

setup_window_table() {
    # Set up an "x-windows" table so that we can
    # kinda move our our addr shortcuts as a part of that table
    tmux bind-key W "switch-client -Twin"
}

setup_music() {
    tmux bind-key -Twin F5 'run-shell "musiccontrol toggle"'
    tmux bind-key -Twin F6 'run-shell "musiccontrol stop"'
    tmux bind-key -Twin F7 'run-shell "musiccontrol prev"'
    tmux bind-key -Twin F8 'run-shell "musiccontrol next"'
}

setup_volume() {
    # This relies on the music table being setup
    tmux bind-key -Twin F9 'run-shell "volumecontrol toggle"'
    tmux bind-key -Twin F10 'run-shell "volumecontrol down"'
    tmux bind-key -Twin F11 'run-shell "volumecontrol up"'
}

setup_backlight() {
    # I use acpilight as my backlight driver, which allows my backlight to be
    # controlled while in the tty
    tmux bind-key -Twin F3 'run-shell "xbacklight -dec 10"'
    tmux bind-key -Twin F4 'run-shell "xbacklight -inc 10"'
}

main() {
    setup_status_bar_toggle
    setup_window_table
    setup_music
    setup_volume
    setup_backlight
}

main
# vim: ft=sh
